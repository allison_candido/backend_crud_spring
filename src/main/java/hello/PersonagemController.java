package hello;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.*;
import hello.Personagem;
import hello.PersonagemRepository;

@Controller
public class PersonagemController {
    @Autowired
    private PersonagemRepository personagemRepo;
    
    @CrossOrigin
    @RequestMapping(method=RequestMethod.POST, path="/insert")
    public @ResponseBody String addNewPersonagem (@RequestParam String name){
        Personagem personagem = new Personagem();
        personagem.setNome(name);
        personagemRepo.save(personagem);
        return "O personagem " + personagem.getNome() + " com ID: " + personagem.getId() + " foi criado.";
    }
    @CrossOrigin
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Personagem> getAllPersonagens() {
        return personagemRepo.findAll();
    }
    @CrossOrigin
    @GetMapping(path="/search")
    public @ResponseBody Personagem getPersonagem(@RequestParam Integer id) {
        return personagemRepo.findById(id).get();
    }
    @CrossOrigin
    @RequestMapping(method=RequestMethod.DELETE, path="/delete")
    public @ResponseBody String deletePersonagem (@RequestParam String id){
        if(personagemRepo.existsById(Integer.parseInt(id))){
            Optional <Personagem> personagem_opt = personagemRepo.findById(Integer.parseInt(id));
            Personagem personagem = personagem_opt.get();
            personagemRepo.deleteById(Integer.parseInt(id));
            return "O personagem " + personagem.getNome() + " com ID: " + personagem.getId() + " foi deletado.";
        }else{
            return "O personagem com ID: " + id + " não existe";
        }
    }
    @CrossOrigin
    @RequestMapping(method=RequestMethod.POST, path="/update")
    public @ResponseBody String updatePersonagem (@RequestParam String id, @RequestParam String newName){
        if(personagemRepo.existsById(Integer.parseInt(id))){
            Optional <Personagem> personagem_opt = personagemRepo.findById(Integer.parseInt(id));
            Personagem personagem = personagem_opt.get();
            String old_name = personagem.getNome();
            personagem.setNome(newName);
            personagemRepo.save(personagem);
            return "O personagem: " + old_name + " foi atualizado com o novo nome: " + personagem.getNome() + " com ID: " + personagem.getId();
        }else{
            return "O personagem com ID: " + id + " não existe";
        }
    }
    
}
